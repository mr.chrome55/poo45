public class Depense{
   private Personne personne;
   private double montant;
   private String produit;
   public Depense(Personne personne, String produit, double montant){
       this.personne = personne;
       this.montant = montant;
       this.produit = produit;
   }


    public Personne getPersonne(){
        return this.personne;
    }

    public double getMontant(){
        return this.montant;
    }

    public String getProduit(){
        return this.produit;
    }

    public String toString(){
        return this.personne.getNom()+ "à acheté pour " + this.getMontant()+ "€ de " + this.getProduit();
    }

}