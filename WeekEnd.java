import java.util.List;
import java.util.ArrayList;

public class WeekEnd {
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;

   public WeekEnd(){
       this.listeAmis = new ArrayList<>();
       this.listeDepenses = new ArrayList<>();
   }

   public void addPersonne(Personne personne){
       
   }

   public void addDepense(Personne personne, String produit, double montant){
       listeDepenses.add(new Depense(personne, produit, montant));
   }

    // totalDepensesPersonne prend paramètre une personne
    // et renvoie la somme des dépenses de cette personne.

    public List<Personne> getPersonne(){
        return this.listeAmis;
    }

   public double totalDepensesPersonne(Personne personne){
       return 3;
   }

    // totalDepenses renvoie la somme de toutes les dépenses.
   public double totalDepenses(){
       return 4;
   }

    // depensesMoyenne renvoie la moyenne des dépenses par
    // personne
   public double depensesMoyenne(){
       return 5;
   }

    // depenseProduit prend un produit en paramètre et renvoie la
    // somme des dépenses pour ce produit.
    // (du pain peut avoir été acheté plusieurs fois...)
   public double depenseProduit(String produit){
       return 6;
   }

    // avoirPersonne prend en paramètre une personne et renvoie
    // son avoir pour le week end.
   public double avoirPersonne(Personne personne){
       return 7;
    }
}
