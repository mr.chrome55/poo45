public class ExecutableWeekend{

	public static void main(String [] args){
	// Pierre a acheté du pain pour 12 euros, Paul a dépensé 100 euros pour les pizzas, Pierre a payé l’essence et en a eu pour 70 euros, Marie a acheté du vin pour 15 euros, Paul a aussi acheté du vin et en a eu pour 10 euros, Anna n’a quant à elle rien acheté.
	WeekEnd we1 = new WeekEnd();

	Personne pierre = new Personne("Pierre");
	we1.addPersonne(pierre);

	Personne paul = new Personne("Paul");
	we1.addPersonne(paul);

// we1.addPersonne("Marie");
// we1.addPersonne("Anna");

	we1.addDepense(pierre, "pain", 1200);

	we1.addDepense(paul, "pain", 1200);

	System.out.println(paul.getNom());
	System.out.println(we1.getPersonne());
	System.out.println(we1.totalDepenses());
	System.out.println(we1.totalDepensesPersonne(pierre));
	System.out.println(we1.depensesMoyenne());
	System.out.println(we1.depenseProduit("vin"));
	}
}
